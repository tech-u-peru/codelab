package edu.pe.techu.codelabs.repository;

import edu.pe.techu.codelabs.models.Libro;

import java.util.List;

public interface LibroRepository {
    List<Libro> findAll();
    public void saveLibro(Libro lib);
}
