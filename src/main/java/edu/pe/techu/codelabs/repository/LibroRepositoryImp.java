package edu.pe.techu.codelabs.repository;

import edu.pe.techu.codelabs.models.Libro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class LibroRepositoryImp implements  LibroRepository{

    private final MongoOperations mongoOperations;
    @Autowired
    public LibroRepositoryImp(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }

    @Override
    public List<Libro> findAll() {
        List<Libro>libros = this.mongoOperations.find(new Query(), Libro.class);
        return libros;
    }

    @Override
    public void saveLibro(Libro lib) {
        this.mongoOperations.save(lib);
    }
}
