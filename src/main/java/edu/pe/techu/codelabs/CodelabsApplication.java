package edu.pe.techu.codelabs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodelabsApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodelabsApplication.class, args);
	}

}
