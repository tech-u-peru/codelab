package edu.pe.techu.codelabs.controllers;

import edu.pe.techu.codelabs.models.Libro;
import edu.pe.techu.codelabs.services.LibroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("libros")
public class LibroController {
    private final LibroService libroService;
    private Libro libro;

    @Autowired
    public LibroController(LibroService libroService) {
        this.libroService = libroService;
    }
    @GetMapping()
    public ResponseEntity<List<Libro>> libros(){
        System.out.println("Lista libro");
        return ResponseEntity.ok(libroService.findAll());
    }
    @PostMapping()
    public void saveLibro(@RequestBody Libro lib) {
        libroService.saveLibro(lib);
    }
}
