package edu.pe.techu.codelabs.services;

import edu.pe.techu.codelabs.models.Libro;

import java.util.List;

public interface LibroService {
    List<Libro> findAll();
    public void saveLibro(Libro lib);
}
