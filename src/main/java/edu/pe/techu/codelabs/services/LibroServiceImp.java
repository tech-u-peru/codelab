package edu.pe.techu.codelabs.services;

import edu.pe.techu.codelabs.models.Libro;
import edu.pe.techu.codelabs.repository.LibroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("LibroService")
@Transactional
public class LibroServiceImp implements LibroService{
    private LibroRepository libroRepository;

    @Autowired
    public LibroServiceImp(LibroRepository libroRepository) {
        this.libroRepository = libroRepository;
    }
    @Override
    public List<Libro> findAll() {
        return libroRepository.findAll();
    }

    @Override
    public void saveLibro(Libro lib) {
        libroRepository.saveLibro(lib);
    }
}
