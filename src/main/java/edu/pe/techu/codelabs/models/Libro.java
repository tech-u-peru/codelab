package edu.pe.techu.codelabs.models;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
@Document(collection = "libros")

@JsonPropertyOrder({"idLibro","Titulo","autor","genero","fechaPublicacion"})
public class Libro implements Serializable {
    private String idLibro;
    private String Titulo;
    private String autor;
    private String genero;
    private String fechaPublicacion;

    public Libro(){}

    public Libro( String idLibro, String titulo, String autor,String genero, String fechaPublicacion){
        this.setIdLibro(idLibro);
        this.setTitulo(titulo);
        this.setAutor(autor);
        this.setGenero(genero);
        this.setFechaPublicacion(fechaPublicacion);
    }

    public String getIdLibro() {
        return idLibro;
    }

    public void setIdLibro(String idLibro) {
        this.idLibro = idLibro;
    }

    public String getTitulo() {
        return Titulo;
    }

    public void setTitulo(String titulo) {
        Titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setFechaPublicacion(String fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }
}
